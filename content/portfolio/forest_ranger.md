---
title: Forest Ranger
description: Illegal Logging Alarm project for ARM
draft: true
weight: 4
---

Illegal logging is a problem that plagues forests across the world. I was part of a team prototyping a system to assist with this problem in  my 3rd year of university. 

We created a device, based on the raspberry pi zero, to be mounted on at-risk trees. The device would detect the tree falling if it were cut and report to a base station. Finally, we created a platform to monitor possible deforestation events as detected by these devices.

The platform was based on *Firebase*, and used *Typescript* and *React* on the frontend.
